# from django.urls import path, include
# from rest_framework import routers
# from .views import UserViewSet

# router = routers.DefaultRouter()
# router.register(r'usuarios', UserViewSet)

# urlpatterns = [
#     path('', include(router.urls)),
# ]

from rest_framework import routers
from .views import UserViewSet

router = routers.DefaultRouter()
router.register(r'users', UserViewSet, basename='users')


urlpatterns = router.urls
