# from django.urls import path, include
# from .views import ProductosAPIView, ProductoAPIView

# urlpatterns = [
#    path('productos/', ProductosAPIView.as_view()),
#    path('productos/<int:id>', ProductoAPIView.as_view())
# ]

from rest_framework import routers
from .views import ProductosViewSet

router = routers.DefaultRouter()
router.register(r'productos', ProductosViewSet, basename='productos')

urlpatterns = router.urls

