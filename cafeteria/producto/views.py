# from django.shortcuts import render
# from django.http import JsonResponse, HttpResponse
# from rest_framework import status
# from .serializers import ProductoSerializer
# from rest_framework.decorators import api_view
# from rest_framework.response import Response
# from .models import Producto
# from rest_framework.views import APIView
# from rest_framework import mixins
# from rest_framework import generics

# # Create your views here.

# class ProductosAPIView(APIView):
#     def get(self, request):
#         productos = Producto.objects.all()
#         ordenar_por = request.GET.get('ordenar_por', '')
#         if ordenar_por:
#             productos = productos.order_by(ordenar_por)
#         respuesta = ProductoSerializer(productos, many = True)

#         return Response(respuesta.data)
    
#     def post(self, request):
#         serializer = ProductoSerializer(data = request.data)
#         if serializer.is_valid():
#             serializer.save()
#             return Response(serializer.data, status = status.HTTP_201_CREATED)
#         return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

# class ProductoAPIView(mixins.RetrieveModelMixin, mixins.UpdateModelMixin,
#                        mixins.DestroyModelMixin, generics.GenericAPIView):#esto no funciona, solo el get by id, el resto no anda
    
#     queryset = Producto.objects.all()
#     serializer_class = ProductoSerializer
#     lookup_field = 'id'

#     def get(self, request, *args, **kwargs):
#         return self.retrieve(request, *args, **kwargs)
    
#     def update(self, request, *args, **kwargs):
#         return self.update(request, *args, **kwargs)#method not allowed

#     def delete(self, request, *args, **kwargs):
#         return self.delete(request, *args, **kwargs)#maximum recursion depth exceeded in comparison

from rest_framework import viewsets
from .models import Producto
from .serializers import ProductoSerializer
from cafeteria.permissions import IsRecepcionista

class ProductosViewSet(viewsets.ModelViewSet): #funciona para GET, GET by id, POST, PUT y DELETE by id
    serializer_class = ProductoSerializer
    queryset = Producto.objects.all()
    permission_classes = [IsRecepcionista]
    #se pueden sobreescribir la logica de las funciones internas del ModelViewSet
    #def retrieve(****):

