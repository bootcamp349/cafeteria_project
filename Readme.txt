https://github.com/bootcamp-mitic-fpuna/cafeteria_be
            PROYECTO CAFETERIA

#-----------------------------------#
comandos utilizados para el proyecto
#-----------------------------------#
python -m venv .venv
.\.venv\Scripts\activate
python.exe -m pip install --upgrade pip
pip install -r requirements.txt

django-admin startproject cafeteria .
                #configurar cafeteria.settings
                	# DATABASES 
			# INSTALLED_APPS
			#CORS_ALLOW_ALL_ORIGINS *TRUE
			#REST_FRAMEWORK
			#SIMPLE_JWT
			#MIDDLEWARE *corsheaders.middleware.CorsMiddleware


python .\manage.py startapp productos
python .\manage.py strarapp pedidos
                #configurar cafeteria.settings
                    #agregar 'productos' a INSTALLED_APPS
                    # aggregar 'pedidos' a INSTALLED_APPS
		    # aggregar 'usuarios' a INSTALLED_APPS
		    # aggregar 'usuarios' a INSTALLED_APPS
		    #agregar 'rest_framework' a INSTALLED_APPS
		    #agregar 'rest_framework_simplejwt' a INSTALLED_APPS
		    #agregar 'djongo' a INSTALLED_APPS
		    #agregar 'djongo' a INSTALLED_APPS	            
		    #agregar 'corsheaders' a INSTALLED_APPS

#-----------------------------------#
Modelos utilizados para el proyecto
#-----------------------------------#
Producto
Pedido
Usuario

En el archivo settings se hacen los cambios necesarios para poder interactuar con MONGOdb
#DATABASES
Utilizamos archivos JSON para leer y escribir datos de la base de datos
El frontend esta hecho con la libreria React  y esta estructurado de la siguiente manera.
main -> app -> login -> home -> paginas
Todos los componentes estan en la carpeta componentes
El backend esta hecho con Django rest framework
El codigo backend lo realice siguiendo las clases, utilizando los ViewSets de rest framework

PAGINAS PARA RESOLVER DUDAS


https://www.django-rest-framework.org/tutorial/6-viewsets-and-routers/
https://www.django-rest-framework.org/topics/documenting-your-api/
https://www.django-rest-framework.org/api-guide/generic-views/
https://ead.pol.una.py/politecnica/pluginfile.php/451938/mod_resource/content/1/Cafeteria%20API.pdf
https://gitlab.com/bootcamp335/cafeteria
https://www.youtube.com/watch?v=UpsZDGutpZc
https://www.djongomapper.com/integrating-django-with-mongodb/
https://docs.djangoproject.com/en/4.1/contents/
https://www.mongodb.com/compatibility/mongodb-and-django
https://www.youtube.com/watch?v=I17uA1sVQ2g

Tipo de vista utilizada
VIEWSETS
