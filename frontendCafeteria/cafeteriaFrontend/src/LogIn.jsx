import { useState } from 'react'
import coffeeIcon from './assets/coffee.png'
import jwtDecode from 'jwt-decode'

const LogIn = ({ onLogin }) => {
  const [username, setUsername] = useState('')
  const [password, setPassword] = useState('')

  const loginHandle = (e) => {
    e.preventDefault()
    // login and get an user with JWT token
    fetch('http://localhost:8000/api/token/', {
      method: 'POST',
      headers: { 'Content-Type': 'application/json'},
      body: JSON.stringify({
        username,
        password,
      })
    })
      .then((res) => res.json())
      .then((tokenData) => {
        window.localStorage.setItem('accessToken', JSON.stringify(tokenData.access))
        console.log(tokenData);
        console.log(jwtDecode(tokenData.access).user_id);
        onLogin(jwtDecode(tokenData.access).user_id)
      })
  }

  return (
    <>
      <form onSubmit={loginHandle}>
        <img src={coffeeIcon} alt="Coffee Icon" width={100} />
        <h2>Coffee Time</h2>
        <p>Para ingresar, digite su usuario y clave</p>
        <p>Use atuny0 / 9uQFF1Lh si no tiene una cuenta</p>
        <div>
          <input
            aria-label="Username"
            placeholder="Usuario"
            id="username"
            type="text"
            onChange={(e) => {
              setUsername(e.target.value)
            }}
          />
        </div>
        <div>
          <input
            aria-label="Password"
            placeholder="Contrasenha"
            id="password"
            type="password"
            onChange={(e) => {
              setPassword(e.target.value)
            }}
          />
        </div>
        <div>
          <button type="submit">Login</button>
        </div>
      </form>
    </>
  )
}

export default LogIn
