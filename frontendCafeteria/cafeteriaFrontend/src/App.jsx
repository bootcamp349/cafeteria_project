
import { useState, useEffect } from 'react'
import './App.css'
import Home from './Home'

import LogIn from './LogIn'
import jwtDecode from 'jwt-decode'


const pedidos = [
	{
		"id": 1,
		"mesa": 3,
		"lista_productos": [
			1,
			3,
			4
		]
	},
	{
		"id": 2,
		"mesa": 1,
		"lista_productos": [
			4,
			5
		]
	},
	{
		"id": 3,
		"mesa": 6,
		"lista_productos": [
			3
		]
	},
	{
		"id": 4,
		"mesa": 10,
		"lista_productos": [
			1,
			2,
			4
		]
	}
]

function App() {
		const [userId, setUserId] = useState(null)

		useEffect(() => {
		  const token = window.localStorage.getItem('accessToken')
		  if (token){
			setUserId(jwtDecode(JSON.parse(token)).user_id)
		  }
		}, [])
		
		const onLoginHandler = (userId) => {
		  console.log(userId)
		  setUserId(userId)
		}
		
		const onLogoutHandler = () => {
		  setUserId(null)
		  window.localStorage.removeItem('accessToken')
		}
		
		return (
		  <>
			{userId ? (
			  <Home onLogout={onLogoutHandler} userId={userId} />
			) : (<LogIn onLogin={onLoginHandler} />)
			}
		  </>
	)
}

export default App

{/* <div style={{display: 'flex', gridGap: '5px'}} className='App'>
<Pedidos
  id = {pedido.id}
  mesa = {pedido.mesa}>
</Pedidos>
</div> */}




// const listItems = pedidos.map((pedido) => {
// 	return (
// 		<Pedidos key={pedido.id}{...pedido}/>
// 	)
// })
// //Mapea todo el vector de pedidos, retornando el renderizado de la funcion pedidos con el prop pedido, que contiene un elemento de pedidos
// //El elemento padre debe tener como atributo un key, un identificador unico para cada nodo
// //El metodo filter nos puede ayudar para retornar solo aquellos pedidos para preparar
// return(
// 	<div style={{display:"flex", gridGap:"50px"}}>
// 		{listItems}
// 	</div>
// )


{/* <BrowserRouter>
<Routes>
	<Route path='/pedidos' element={<PedidosList/>}/>
</Routes>
</BrowserRouter> */}