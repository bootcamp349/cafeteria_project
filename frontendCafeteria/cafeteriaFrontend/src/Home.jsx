import { useState, useEffect } from 'react'

import Pedidos from './componentes/ListaPedidos'
//import Productos from './componentes/ListaProductos'
//import FormularioPedido from './componentes/FormularioPedido'

const Home = ({ onLogout, userId }) => {
  
    const [user, setUser] = useState(userId)

    useEffect(() => {
        fetch('http://localhost:8000/users/' + userId, {
        method: 'GET' /* or POST/PUT/PATCH/DELETE */,
        headers: {
            Authorization: `Bearer ${JSON.parse(window.localStorage.getItem('accessToken'))}`,
            'Content-Type': 'application/json',
        },
        })
        .then((res) => res.json())
        .then((userData) => {
            setUser(userData)
        })
    }, [])

    const [pedidos, setPedidos] = useState([])
    useEffect(() => {
        console.log('page loaded');
        fetch('http://localhost:8000/pedidos/', {
            method: 'GET' /* or POST/PUT/PATCH/DELETE */,
            headers: {
                Authorization: `Bearer ${JSON.parse(window.localStorage.getItem('accessToken'))}`,
                'Content-Type': 'application/json',
            },
        })
            .then((res) => res.json())
            .then((data) => {
                setPedidos(data)
            })
    }, [])

    const logoutHandler = () => {
        onLogout()
    }

    const role = user ? user.group_name : null

    const content = role && role === 'recepcionista' ? <Productos /> : <p>Home Page</p>

    return (
        <>  
            <button onClick={logoutHandler}>Logout</button>
            {user && <>
                <h1>Bienvenido {user.username}!</h1>
                <p>{user.group_name}</p>
            </>}
            <Pedidos/>        
    </>
    )
}

export default Home