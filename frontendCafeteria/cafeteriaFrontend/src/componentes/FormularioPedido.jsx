import { useState, useEffect } from "react"
const initialForm ={
    mesa:0,
    listaPedidos:[]
}
export default function FormularioPedido(){
    const [form, setForm] = useState(initialForm)
    const [productos, setProductos] = useState([])

    useEffect(() => {
        fetch('http://localhost:8000/productos/',{
            method: 'GET' /* or POST/PUT/PATCH/DELETE */,
            headers: {
                Authorization: `Bearer ${JSON.parse(window.localStorage.getItem('accessToken'))}`,
                'Content-Type': 'application/json',
            },
        })
            .then((res) => res.json())
            .then((data) => {
                setProductos(data)
            })
    }, [])
    const pedidoHandler =(e)=>{
        
    }
    const submitHandler =(e)=>{

    }
    const resetHandler =(e)=>{

    }

    return(
        <>
            <h2>Tomar Pedido</h2>
            <form onSubmit={submitHandler}>
                <input
                type="number" 
                name="mesa" 
                placeholder="Nro de mesa" 
                onChange={pedidoHandler} 
                value={form.mesa}/>

                {productos.map((item)=>{
                    return(
                        <>
                            <div>
                                <label htmlFor={item.id}>{item.nombre}</label>
                                <input 
                                type="checkbox" 
                                id={item.id} 
                                onChange={pedidoHandler}/>
                            </div>
    
                        </>
                    )
                })}
                <input 
                type="submit" 
                value="Aceptar"/>
                <input 
                type="reset" 
                value="Restablecer"
                onClick={resetHandler}/>
            </form>
        </>
    )
}

{/* <p>Elige el producto</p>
{productos.map((item) => {
    return(
        <>
            <div>       
                <input type="radio" id={item.nombre} value={item.nombre} onChange={e=> setProducto(e.target.value)}/>
                <label htmlFor={item.nombre}>{item.nombre}</label>
            </div> 
        </>
    )
    })
}           
</> */}


// <div>    
// <div>
//     <form onSubmit={submitHandler}>
//         <input type="text" name="mesa" placeholder="Nro de mesa" onChange={pedidoHandler} value={form.mesa}/>
//         <input type="submit" value="Ok"/>
//         <input type="reset" value="Limpiar"/>
//     </form>
// </div>
// <p>Elige los productos</p>
// {productos.map((item)=>{
//     return(
//         <>
//             <div>
//                 <label htmlFor={item.nombre}>{item.nombre}</label>
//                 <input type="checkbox" id={item.nombre} onChange={pedidoHandler}/>
//             </div>

//         </>
//     )
// })}
// <input type="submit" value="Tomar Pedido"/>
// <input type="reset" value="Limpiar"/>
// </div>
// </>


