import { useState, useEffect } from 'react'

const Producto = ({id = props.id}) => {
    const [producto, setProducto] = useState([])

    useEffect(() => {
        fetch('http://localhost:8000/productos/' + id,{
            method: 'GET' /* or POST/PUT/PATCH/DELETE */,
            headers: {
                Authorization: `Bearer ${JSON.parse(window.localStorage.getItem('accessToken'))}`,
                'Content-Type': 'application/json',
            },
        })
            .then((res) => res.json())
            .then((data) => {
                setProducto(data)
            })
    }, [])

    return (
        <>
            <h3>{producto.nombre} - {producto.precio}</h3>
        </>
    )
}

export default Producto
