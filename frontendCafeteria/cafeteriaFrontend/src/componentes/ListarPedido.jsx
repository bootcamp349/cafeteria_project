import { useState } from "react"

const ObtenerTotal =(
    listaProductos = props.listaIds)=>{
    const [precio, setPrecio] = useState(0)

    for(var i = 0; i < listaProductos.length; i++){
        const [producto, setProducto] = useState([])
        useEffect(() => {
            fetch('http://localhost:8000/productos/' + listaProductos[i],{
                method: 'GET' /* or POST/PUT/PATCH/DELETE */,
                headers: {
                    Authorization: `Bearer ${JSON.parse(window.localStorage.getItem('accessToken'))}`,
                    'Content-Type': 'application/json',
                },
            })
                .then((res) => res.json())
                .then((data) => {
                    setProducto(data)
                })
        }, [])
        setPrecio(precio + producto.precio)
    }
    return (
        <>
            <h3>Total = {precio}</h3>
        </>
    )

}

export default ObtenerTotal

// const [producto, setProducto] = useState([])
// useEffect(() => {
//     fetch('http://localhost:8000/productos/' + productoId,{
//         method: 'GET' /* or POST/PUT/PATCH/DELETE */,
//         headers: {
//             Authorization: `Bearer ${JSON.parse(window.localStorage.getItem('accessToken'))}`,
//             'Content-Type': 'application/json',
//         },
//     })
//         .then((res) => res.json())
//         .then((data) => {
//             setProducto(data)
//         })
// }, [])
// setPrecio(precio + producto.precio)
// return (
//     <>
//         <h3>Total = {precio}</h3>
//     </>
// )