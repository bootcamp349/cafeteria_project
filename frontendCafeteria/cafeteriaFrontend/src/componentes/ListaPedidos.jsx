import { useState, useEffect } from 'react'
import Producto from './Producto'
//import ObtenerTotal from './ListarPedido'
const Pedidos = () => {
    const [pedidos, setPedidos] = useState([]) 
    useEffect(() => {
        fetch('http://localhost:8000/pedidos/',{
            method: 'GET' /* or POST/PUT/PATCH/DELETE */,
            headers: {
                Authorization: `Bearer ${JSON.parse(window.localStorage.getItem('accessToken'))}`,
                'Content-Type': 'application/json',
            },
        })
            .then((res) => res.json())
            .then((data) => {
                setPedidos(data)
            })
    }, [])
    return (
        <>
            <h2>Pedidos</h2>
            <ul>
                {pedidos.map((pedido) => {
                    return <li>Mesa {pedido.mesa}{pedido.lista_productos.map((idProducto)=> <Producto id={idProducto} key = {idProducto}/>)}</li>
                })}
            </ul>
        </>
    )
}

export default Pedidos
